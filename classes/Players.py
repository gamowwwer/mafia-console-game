class Player:
    def __init__(self, number, name, role='c'):
        self.alive = True
        self.name = name
        self.number = number
        self.role = role

    def apply_role(self, role):
        self.role = role

    def kill(self):
        self.alive = False

    def revive(self):
        self.alive = True

    def __str__(self):
        return f'{self.number}, {self.name}'

    def __repr__(self):
        return f'\"{self.number}, {self.name}\"'


class Mafia(Player):
    def __init__(self):
        super().__init__('m')


class Commissar(Player):
    def __init__(self):
        super().__init__('p')


class Hooker(Player):
    def __init__(self):
        super().__init__('h')


class Doctor(Player):
    def __init__(self):
        super().__init__('d')


class Maniac(Player):
    def __init__(self):
        super().__init__('m')
