class Clan:
    def __init__(self, count=1):
        self.count = count
        self.members = []

    def add_player(self, player):
        self.members.append(player)

    def remove_player(self, player):
        if player in self.members:
            self.members.remove(player)


class MafiaClan(Clan):
    pass


class CommissarClan(Clan):
    pass


class ManiacClan(Clan):
    pass
