from classes.Players import Player
import classes.Clans as clans
from json import load


class Game:
    def __init__(self):
        self.players = dict()
        self.player_num = 0
        self.layout = []
        self.mafia_clan = None
        self.police_clan = None
        self.maniac_clan = None
        self.doctor_clan = None

    def __add_players(self):
        print('Add players manually or read or read from file? (:m/:r) ')
        while True:
            input_str = input()
            if input_str == ':r':
                file_name = r'd:\WORK\workspace\Python\mafiaProject\resources\players.txt'
                with open(file_name, "r", encoding='utf-8') as f:
                    self.players = load(f)
                    self.players = {int(k): v for k, v in self.players.items()}
                    print(self.players)
                    break
            elif input_str == ':m':
                while True:
                    input_player = input("Please provide player like this: <player_number>, <player_name>:\n")
                    player_init = input_player.split(', ', maxsplit=1)
                    try:
                        player_num = player_init[0]
                        player_name = player_init[1]
                        player = Player(player_num, player_name)
                        print(f"Player \"{player}\" added")
                        self.players[player_num] = player
                        while True:
                            input_add = input("Add more players? (:a/:q): ")
                            if input_add == ":a":
                                break
                            elif input_add == ":q":
                                break
                            elif input_add == ':l':
                                print(self.players)
                            else:
                                print("Bad command. try again: ")
                        if input_add == ":q":
                            break
                    except ValueError:
                        print("Bad input. Please try again")
                pass
                break
            else:
                print('Bad command. Command should be :m or :r. Please try again:')

    def __set_layout(self):
        layout_welcome = "Please provide layout (space split):\n" \
                         "m - Mafia, c - Commissar, d - Doctor, h - Hooker, p - Maniac"
        print(layout_welcome)
        while True:
            template = 'mcdhp'
            good_roles = False
            while not good_roles:
                layout_str = input()
                good_roles = True
                self.layout = layout_str.split()
                for role in self.layout:
                    if role not in template:
                        print('Bad layout. Please try again:')
                        good_roles = False
            print("Your layout is: {}".format(self.layout))
            change = input("Change layout? (y/n) ")
            if change == 'y':
                print(layout_welcome)
            if change == 'n':
                break

    def setup(self):
        print("Setup is now in process")
        self.__add_players()
        # self.set_layout()
        for role in self.layout:
            pass

    def run(self):
        print(r'~~~====================~~~')
        print("~ Welcome to Mafia game! ~")
        print(r'~~~====================~~~')
        round = 1
        self.setup()
